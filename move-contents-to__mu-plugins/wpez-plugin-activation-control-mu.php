<?php
/**
 * Plugin Name: WPezPlugins: Plugin Activation Control (folder: mu-plugins)
 * Plugin URI: https://gitlab.com/WPezPlugins/wpez-plugin-activation-control
 * Description: See primary plugin for details.
 * Version: 0.0.0 (Proof of Concept)
 * Author: Mark F. Simchock (Chief Executive Alchemist @ Alchemy United)
 * Author URI: https://AlchemyUnited.com/utm_source=wpez_ft
 * Text Domain: wpez_pac
 * License: GPLv2 or later
 *
 * @package WPezPluginActivationControl
 */

/**
 * -----------------------------------------------------------------------------------------
 * IMPORTANT: THIS IS THE ONLY FILE YOU NEED TO EDIT.
 * -----------------------------------------------------------------------------------------
 */


namespace WPezPluginActivationControl;

defined( 'ABSPATH' ) || exit;

// We want out core functionality to load first, and then our filters here can piggyback off that.
require_once 'wpez-plugin-activation-control-mu-core/wpez-plugin-activation-control-mu-core.php';


/**
 * IMPORTANT: DO NOT CHANGE THE NAME OF THIS FUNCTION.
 *
 * @param string|false $group - The group name or false.
 * @param object|false $get_row - The result from $wpdb->get_row() or false.
 * @param boolean      $get - Return the value or not.
 *
 * @return void|null|array
 */
function pluginsControl( $group, $get_row, bool $get ) {

	// We use a static because we want to save when we set ($ret = false) and get it when we get ($ret = true).
	static $arr_de_active_plugins = null;

	// If the $request URI has a group then it's been logged. And now you can do your plugin control stuff.
	if ( false !== $group && null === $arr_de_active_plugins ) {

		/**
		 * -----------------------------------------------------------------------------------------
		 * IMPORTANT: Your custom code to convert a group value into an array of active plugins starts here
		 * -----------------------------------------------------------------------------------------
		 */

		 // Example - Feel free to customize or delete and add your own logic.
		switch ( $group ) {
			case 'post_is_single':
				$arr_de_active_plugins = array(
					// 'spatie-ray/wp-ray.php',
					// 'wpez-plugin-activation-control-test/wpez-plugin-activation-control-test.php',
					// 'wpez-plugin-activation-control/wpez-plugin-activation-control.php',
				);
				break;
			case 'page_is_single':
				$arr_de_active_plugins = array(
					// 'spatie-ray/wp-ray.php',
					// 'wpez-plugin-activation-control-test/wpez-plugin-activation-control-test.php',
					// 'wpez-plugin-activation-control/wpez-plugin-activation-control.php',
				);
				break;
			case 'some_other_group':
				// STUB.
				break;
			default:
				// we didn't find anything for the $group so ultimately we won't alter the option: active_plugins.
				$arr_de_active_plugins = false;
		}

		/**
		 * -----------------------------------------------------------------------------------------
		 * IMPORTANT: Your custom code ends here
		 * -----------------------------------------------------------------------------------------
		 */
	}

	if ( true === $get ) {
		return $arr_de_active_plugins;
	}
}


add_filter( __NAMESPACE__ . '\setInsertArgs', __NAMESPACE__ . '\setInsertArgs', 10, 4 );
/**
 * With this filter you can customize what gets logged for a given request.
 *
 * @param bool     $bool         - Default: false, which means the instert must be intentional.
 * @param array    $arr_defaults - Keys: 'pac_request' (required), 'pac_request' (required), and 'pac_timestamp' (optional).
 * @param \WP      $wp           - Instance of WP class.
 * @param \WP_Post $post         - Instance of WP_Post class.
 *
 * @return array|false
 */
function setInsertArgs( bool $bool, array $arr_defaults, \WP $wp, \WP_Post $post ) {

	// Use conditional tags - or whatever - to assign a group to the request so and that will be logged.
	// https://codex.wordpress.org/Conditional_Tags

	/**
	 * -----------------------------------------------------------------------------------------
	 * IMPORTANT: Your custom code to define rules that assign a group to a URI starts here
	 * -----------------------------------------------------------------------------------------
	 */

	// Example - Feel free to customize or delete and add your own logic.
	$arr_defaults['pac_group'] = 'is_not_single';
	if ( is_single( $post->ID ) ) {

		$arr_defaults['pac_group'] = $post->post_type . '_is_single';
	}

	/**
	 * -----------------------------------------------------------------------------------------
	 * IMPORTANT: Your custom code ends here
	 * -----------------------------------------------------------------------------------------
	 */

	return $arr_defaults;
}
