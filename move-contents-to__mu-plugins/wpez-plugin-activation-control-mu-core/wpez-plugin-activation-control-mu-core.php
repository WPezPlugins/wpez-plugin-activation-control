<?php
/**
 * -----------------------------------------------------------------------------------------
 * IMPORTANT: You don't need to change anything in this file.
 * -----------------------------------------------------------------------------------------
 */

namespace WPezPluginActivationControl;

use function WPezPluginActivationControl\pacSettings as WPezPluginActivationControlPacSettings;

defined( 'ABSPATH' ) || exit;

/**
 * A function that centralizes the plugin's settings.
 *
 * @param string $key      The setting being requested.
 * @param mixed  $fallback If the setting requested is not found, return this value.
 *
 * @return mixed
 */
function pacSettings( string $key, $fallback = '' ) {

	$arr = array(
		'table_name'      => 'wpez_pac',
		'insert_defaults' => array(
			'pac_request' => false,
			'pac_group'   => false,
		),
		'plugin_slug'     => 'wpez-plugin-activation-control/wpez-plugin-activation-control.php',
	);

	$arr_new = apply_filters( __NAMESPACE__ . 'pac_settings', array(), $arr );
	if ( is_array( $arr_new ) ) {
		$arr = array_merge( $arr, $arr_new );
	}

	if ( isset( $arr[ $key ] ) ) {
		return $arr[ $key ];
	}
	return $fallback;
}


add_action( 'muplugins_loaded', __NAMESPACE__ . '\getRequestStatus', 10, 0 );
/**
 * As early as we can, query the custom table to see if the current request is in the table.
 *
 * @return string|false
 */
function getRequestStatus() {

	global $wpdb;

	// We use a static to stash the value of $group because we query early ('muplugins_loaded') but need the value later ('wp').
	static $group;

	if ( null === $group ) {

		$uri = ( isset( $_SERVER['REQUEST_URI'] ) ) ? wp_unslash( $_SERVER['REQUEST_URI'] ) : false;
		$uri = ltrim( $uri, '/' );
		$uri = rtrim( $uri, '/' );

		$table_name = $wpdb->prefix . pacSettings( 'table_name' );
		$get_row    = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$table_name} WHERE pac_request = %s LIMIT 1", $uri ) );

		// Update the value of $group. Default to false (i.e., we got nuttin' from the DB).
		$group = false;
		// If we have a row, then $group is the value of pac_group.
		if ( $get_row instanceof \stdClass && property_exists( $get_row, 'pac_group' ) && ! empty( $get_row->pac_group ) ) {
			$group = $get_row->pac_group;
		}
		// Now that we know the group "status" of the request, provide a filter that allows us to take action based on this knowledge.
		apply_filters( __NAMESPACE__ . '\setPluginsControl', $group, $get_row );
	}

	return $group;
}

add_filter( __NAMESPACE__ . '\setPluginsControl', __NAMESPACE__ . '\setPluginsControl', 10, 2 );
/**
 * Wrapper around pluginsControl().
 *
 * @param string|false $group
 * @param object|false $get_row
 *
 * @return void
 */
function setPluginsControl( $group, $get_row ) {

	$get = false;
	return pluginsControl( $group, $get_row, $get );
}
/**
 * Wrapper around pluginsControl().
 *
 * @return false|array|null
 */
function getPluginControl() {

	$get = true;
	return pluginsControl( false, false, $get );
}

// https://developer.wordpress.org/reference/functions/get_option/
// See final line in get_option(): apply_filters( "option_{$option}", maybe_unserialize( $value ), $option );
// that's what we're going here.
add_filter( 'option_active_plugins', __NAMESPACE__ . '\filterActivePlugins', PHP_INT_MAX - 10, 2 );
/**
 * We're going to filter the option: active_plugins, and maybe use our own version.
 *
 * @param mixed $value The value of the option.
 * @param sting $option The name of the option.
 *
 * @return array|bool|mixed
 */
function filterActivePlugins( $value, $option ) {

	// Let's be extra careful about what we're going to filter.
	if ( is_admin() || 'active_plugins' !== $option ) {
		return $value;
	}

	$arr_ret = getPluginControl();

	if ( is_array( $arr_ret ) && is_array( $value ) ) {
		// We don't want to disable ourselves. (TODO - Do we?? Probably doesn't matter as that plugin only added the custom table.)
		$str_plugin_slug = WPezPluginActivationControlPacSettings( 'plugin_slug' );
		$ndx = array_search( $str_plugin_slug, $arr_ret, true );
		if ( false !== $ndx ) {
			unset( $arr_ret[ $ndx ] );
		}
		// now remove the blacklisted plugins from the active plugins.
		$arr_ret = array_diff( $value, $arr_ret );

		return $arr_ret;
	}
	return $value;
}


// https://codex.wordpress.org/Conditional_Tags
//
// Can only use conditional tags *after* the action hook: posts_selection 
// https://developer.wordpress.org/reference/hooks/posts_selection/
//
// 

add_action( 'wp', __NAMESPACE__ . '\setRequest', PHP_INT_MAX - 100, 1 );
/**
 * Log - or not - this request to the custom table.
 *
 * @param \WP $wp An instance of the WP object.
 *
 * @return false|integer
 */
function setRequest( \WP $wp ) {

	global $post, $wpdb;

	// Get the status (the value of $group) for the current request.
	$group = getRequestStatus();

	if ( is_string( $group ) && ! empty( $group ) ) {
		// this $request was already added to the custom table.
		return false;
	}

	// Else, we haven't logged this $request yet. Let's (maybe) do it. 
	$arr_defaults = pacSettings( 'insert_defaults' );
	$arr_defaults['pac_request'] = $wp->request;

	// Provide a filter that allows for what's inserted into the custom table to be modified/customized.
	$arr_insert = apply_filters( __NAMESPACE__ . '\setInsertArgs', false, $arr_defaults, $wp, $post );

	// If an array isn't returned we'll presume that this request should not be logged.
	if ( ! is_array( $arr_insert ) ) {
		return false;
	}

	// TODO - improve validation.
	if ( ! isset( $arr_insert['pac_request'], $arr_insert['pac_group'] ) || ! is_string( $arr_insert['pac_request'] ) || ! is_string( $arr_insert['pac_group'] ) ) {
		return false;
	}

	if ( ! isset( $arr_insert['pac_timestamp'] ) ) {
		$arr_insert['pac_timestamp'] = time();
	}

	// Let's insert.
	$result = $wpdb->insert(
		$wpdb->prefix . pacSettings( 'table_name' ),
		array(
			'pac_request'   => $arr_insert['pac_request'],
			'pac_group'     => $arr_insert['pac_group'],
			'pac_timestamp' => $arr_insert['pac_timestamp'],
		),
		array( 'object_id' => '%s' ),
	);

	return $result;
}