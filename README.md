# WPezPluginActivationControl

###A WordPress plugins for controlling the option: active_plugins array (for frontend visits only). 

For example, post_type = post can have Plugins A, B and C active. On the other hand, post_type = page can have Plugins A, C, D and E active. The complexity and sophistication of the rules for which plugins are active when is limited only by your imagination, your knowledge of WP, and your PHP skills.

**Important: This is a experimental / proof of concept WordPress plugin. It also requires basic PHP skills to implement your own rules.**

### Installation

Before you activate this plugin, you'll have to move some folders and files around. 

Within the plugin's main folder (wpez-plugin-activation-control) you will have to move *the contents* of the folder: move-contents-to__mu-plugins to WordPress' wp-content / mu-plugins folder. Then activate the plugin.

*Please to the moving + activating in a timely manner as the DB table used by the code in the mu-plugins folder isn't setup until the plugin is activated.  

Ultimately, you want this structure:  

- wp-content (folder)
    - mu-plugins (folder)
        - wpez-plugin-activation-control-mu.php (file)  **<< FYI - This is the only file you will / should edit.**
        - wpez-plugin-activation-control-mu-core (folder)
            - wpez-plugin-activation-control-mu-core.php (file)

- plugins (folder)
    - *All your other installed plugins*
    - wpez-plugin-activation-control (folder)
        - wpez-plugin-activation-control.php (file) 

Note: You're *not* moving the folder: move-contents-to__mu-plugins, only what's within it. 

**Get the mu-plugins folders and files in place before activating the plugin. There's a function that returns various settings values in one of the mu-plugins' files.** That said, don't make any frontend requests as the code there will look for a DB table that isn't created until the plugins is activated.

### Overview

**In a nutshell...**

- WP gets a request via a URL. 
- We parse that URL and see if we have already logged the URI in our custom table.
- If not, evaluate it, assign it a group value (string) and then log (i.e., insert) that URI into the custom table.
- If we already logged the URI, we don't have to do anything more.
- That is, the first time we see a particular URI there's some overhead (and all plugins end up being active).
- But for all subsequent requests with a logged URI, when WP is getting the option 'plugins_active', we filter that get_option() and return an array of plugins that should be active given the current URI's group (as pulled from the custom table). 

**A more detailed (and perhaps slightly more confusing) step by step...**

- A browser makes a URL-based request of WP. 
    - Within the file wpez-plugin-activation-control-mu.php:
        - Use the 'muplugins_loaded' action hook to:
            - Parse the URL to get the URI
            - Use the value of the URI to query the plugin's custom DB table.
                - If that query finds a row for the URI, use a static variable to store the $group value (string)
                - If we don't find a row, use the same static variable to store the value false (bool), for URI not found
                - Then add a hook: apply_filters setPluginsControl
        - Use the 'wp' action hook to:
            - Get the $group value from the static variable (mentioned a couple lines above)
                 - If it's a non-empty string that means we're already assigned a group to this URI. We're done. We've already logged this URI.
                 - If it's false, then the URI is new (i.e., not logged). We then use our custom rules (likely using WP's conditional tags) to assign a group value to the URI. 
                     - Ultimately, our custom rules will return an array or false. 
                         - If we return false, nothing new will be inserted into the custom table. Returning false is an "off switch"
                         - If we return an array with valid value pairs then that array will be used to insert the URI (and its group value) into the custom table.


    - Within wpez-plugin-activation-control-mu.php use the add_filter pluginsControl to "connect" to setPluginsControl
        - pluginsControl takes the $group value and "converts" it into an array of the plugins that will be activated for that group.
            - This array (or false) is also stored in a static variable ($arr_active_plugins) so we can access it later.

    - Back within the file wpez-plugin-activation-control-mu.php:
        - There's a filter for the get_option() for 'active_plugins'. This filter returns what's stored in the static variable $arr_active_plugins.


### Other Things You Need To Know

- Currently, since the home page will return a URI of '' (i.e., empty, blank, etc.) it can not be evaluated and logged in the custom table.
- ~~There's no blacklisting for which plugins should (not) be active. There is only a (white) list array.~~
- There's no whitelisting for which plugins should be active. A group is an identifier for what (group of) plugins should not be activate for that group.
- That said, don't use get_option('active_plugins') within the function: pluginsControl() (in wpez-plugin-activation-control-mu.php) else you'll get stuck in an infinite loop, as the plugins filter of that get_option call the pluginsControl() function.



### TODOs

- Address the home page issue mentioned above
- Add some settings (e.g., empty the custom DB)
    - Add something to the deactivation hook to remove the table.
- Maybe automate the moving of folders + files to the mu-plugins folder
- ~~Address the blacklist vs whitelist vs infinite loop issue (mentioned above).~~
    - Strictly blacklisting now === KISS - The natural default is for all to be active, we want to define what gets deactivated for a given group.
- Does pluginsControl() need set and get wrappers?
- ~~Address the plugin activation issue vs the stuff in mu-plugins~~
    - Improved, but maybe refine a bit further eventually?
- Cache the custom table queries?
- Refactor using classes (and ditch the static variables)
- Is the plugin self-disabling itself an issue? 
