<?php
/**
 * Plugin Name: WPezPlugins: Plugin Activation Control
 * Plugin URI: https://gitlab.com/WPezPlugins/wpez-plugin-activation-control
 * Description: Adds a custom table (and a helper filter) to log URI requests, plus an additional filter to control the option: active_plugins based on the logging.
 * Version: 0.0.0 (Proof of Concept)
 * Author: Mark F. Simchock (Chief Executive Alchemist @ Alchemy United)
 * Author URI: https://AlchemyUnited.com/utm_source=wpez_pac
 * Text Domain: wpez_pac
 * License: GPLv2 or later
 *
 * @package WPezPluginActivationControl
 */

namespace WPezPluginActivationControl;

defined( 'ABSPATH' ) || exit;


register_activation_hook( __FILE__, __NAMESPACE__ . '\registerActivation' );
/**
 * Adds the custom table to the database.
 *
 * @return false|array
 */
function registerActivation() {

	global $wpdb;

	if ( function_exists( __NAMESPACE__ . '\pacSettings' ) ) {
		
		$charset_collate = $wpdb->get_charset_collate();
		$table_name = $wpdb->prefix . pacSettings( 'table_name' );

		if ( false !== $table_name && ! empty( $table_name ) ) {

			$sql = "CREATE TABLE $table_name (
				pac_id bigint(20) NOT NULL AUTO_INCREMENT, 
				pac_request VARCHAR(255) NOT NULL,
				pac_group VARCHAR(20) NOT NULL,
				pac_timestamp VARCHAR(40) NOT NULL,
				UNIQUE KEY pac_id (pac_id)
			) $charset_collate;";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			return dbDelta( $sql );
		}
	}
	return false;
}

add_action( 'admin_notices', __NAMESPACE__ . '\adminNoticesError' );
/**
 * If the mu-plugins folders + files for the the plugin are not in place, or somehow wrongly setup then issue an admin msg.
 *
 * @return void
 */
function adminNoticesError() {

	if ( ! function_exists( __NAMESPACE__ . '\pacSettings' ) ) {
		?>
		<div class="notice notice-error is-dismissible"><p>
			<?php echo 'WPezPlugins: Plugin Activation Control - The necessary mu-plugins files are missing or not properly installed. Please resolve and reactive the plugin.'; ?>
		</p></div>
		<?php
	}
}


register_deactivation_hook( __FILE__, __NAMESPACE__ . '\registerDeactivation' );
/**
 * Callback for register_deactivation_hook(). Deletes the plugin's options if necessary.
 *
 * @return void
 */
function registerDeactivation() {

	// TODO
}
